CREATE DATABASE FluxoDb
GO
use FluxoDb
GO
CREATE TABLE Companies (   
	Id int IDENTITY (1,1) NOT NULL PRIMARY KEY,
    CompanyName varchar(255),
    CompanyCode varchar(30)
);
GO
CREATE TABLE Users (   
	Id int IDENTITY (1,1) NOT NULL PRIMARY KEY,
    Username varchar(255),
    Password varchar(255),
    Role varchar(50),
    Email varchar(200),
    CompanyCode varchar(30)
);
GO
CREATE TABLE Flow (
    Id int IDENTITY (1,1) NOT NULL PRIMARY KEY,
    Value NUMERIC(20,5),
	Type varchar(10),
	Username varchar(255),
	CompanyCode varchar(30),
	Added DATETIME NOT NULL DEFAULT GETDATE()
);
GO
CREATE TABLE Balance (
    Id int IDENTITY (1,1) NOT NULL PRIMARY KEY,
    Value varchar(255),
    Username varchar(255),
    CompanyCode varchar(30),
	Added DATETIME NOT NULL DEFAULT GETDATE()
);
# teste_conciliacao

## Descrição do Projeto
Esse projeto tem como objeto facilitar o fluxo de caixa diário e apresentar o saldo contabilizado do dia.

Segue na base do projeto uma imagem "fluxo_caixa.jpg" que apresenta o desenho arquitetural da solução.

A ideia nessa solução de ter foco em micro-serviços, com processamentos 'pesados' de forma asincrona para que se no futuro houver necessidade possa ser escalado verticalmente, assim como implantado em uma cloud.

Foram adotadas as seguintes tecnologias:
    - SQL Server 2022 (rodando via docker)
    - RabbitMQ (rodando via docker)
    - Redis (rodando via rocker)

- Existe uma pasta scripts que contem o script para criar o banco e as tabelas necessárias.
* Os outros items são configurações padroes em docker para subir os serviços.


﻿using FluxoCaixa.Core.Interfaces;
using FluxoCaixa.Infraestructure.Request;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Text.Json;

namespace FluxoCaixa.Balance.Web.Controllers
{
    [ExcludeFromCodeCoverage]
    public class ReportController : Controller
    {
        readonly IReportService _reportService;
        public ReportController(IReportService reportService)
        {
            _reportService = reportService;
        }

        /// <summary>
        /// basic create item
        /// </summary>
        /// <param name="basic"></param>
        /// <returns></returns>
        [Route("gerar")]
        [Authorize(Roles = "employee")]
        [HttpPost]
        public ActionResult Create(BalanceRequest balance)
        {
            try
            {
                _reportService.SendMessage(balance);
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}

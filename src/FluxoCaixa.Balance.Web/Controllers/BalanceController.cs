﻿using FluxoCaixa.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics.CodeAnalysis;

namespace FluxoCaixa.Contabilizado.Web.Controllers
{
    [ExcludeFromCodeCoverage]
    public class BalanceController : Controller
    {
        private readonly IBalanceService _balanceService;
        private readonly IReportService _reportService;
        public BalanceController(IBalanceService balanceService, IReportService reportService)
        {
            _balanceService = balanceService;
            _reportService = reportService;
        }

        [Route("saldo")]
        [Authorize(Roles = "employee")]
        [HttpGet]
        public ActionResult<dynamic> Get([FromQuery] string CompanyCode, [FromQuery] DateTime Date)
        {
            var balance = _balanceService.GetByDateAsync(CompanyCode, Date).Result;

            if (balance == null)
                return NotFound(new { message = "Data inválidas" });

            // Retorna os dados
            return new
            {
                saldo = balance.Value,
                date = balance.Added
            };
        }
        [Route("relatorio")]
        [Authorize(Roles = "employee")]
        [HttpGet]
        public IActionResult GetReport([FromQuery] string CompanyCode, [FromQuery] DateTime Date)
        {
            var balance = _reportService.GetReportAsync(CompanyCode).Result;

            if (balance == null)
                return NotFound(new { message = "Data inválidas" });

            // Retorna os dados
            return Ok(balance);
        }
    }
}

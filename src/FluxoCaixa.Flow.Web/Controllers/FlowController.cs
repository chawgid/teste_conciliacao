﻿using FluxoCaixa.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics.CodeAnalysis;

namespace FluxoCaixa.Flow.Web.Controllers
{
    [ExcludeFromCodeCoverage]
    public class FlowController : Controller
    {
        private readonly IFlowService _flowService;
        public FlowController(IFlowService flowService)
        {
            _flowService = flowService;
        }

        [Route("fluxos")]
        [Authorize(Roles = "employee")]
        [HttpGet]
        public IActionResult GetAll([FromQuery] string CompanyCode, [FromQuery] DateTime Date)
        {
            var flows = _flowService.GetAllByDateAsync(CompanyCode, Date).Result;

            if (!flows.Any())
                return NotFound(new { message = "Data inválida" });

            // Retorna os dados
            return Ok(flows);
        }

        [Route("create")]
        [Authorize(Roles = "employee")]
        [HttpPost]
        public ActionResult Create([FromBody] List<Infraestructure.Models.Flow> flows)
        {
            if (flows is null)
            {
                return BadRequest();
            }

            var result = _flowService.AddAsync(flows).Result;

            if (result == 0)
            {
                return BadRequest();
            }

            return Ok();
        }
    }
}

﻿using Microsoft.Extensions.Caching.Distributed;

namespace FluxoCaixa.Infraestructure.Helpers
{
    public interface ICacheHelper
    {
        Task<T?> GetRecordAsync<T>(IDistributedCache cache, string recordId);
        Task SetRecordAsync<T>(IDistributedCache cache, string recordId, T data);
    }
}
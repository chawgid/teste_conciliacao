﻿using Microsoft.Extensions.Caching.Distributed;
using System.Text.Json;

namespace FluxoCaixa.Infraestructure.Helpers
{
    public class CacheHelper : ICacheHelper
    {
        public async Task SetRecordAsync<T>(IDistributedCache cache,
                                                   string recordId,
                                                   T data)
        {
            var options = new DistributedCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(12)
            };

            var jsonData = JsonSerializer.Serialize(data);
            await cache.SetStringAsync(recordId, jsonData, options);
        }

        public async Task<T?> GetRecordAsync<T>(IDistributedCache cache,
                                                       string recordId)
        {
            var jsonData = await cache.GetStringAsync(recordId);

            if (jsonData is null)
            {
                return default;
            }

            return JsonSerializer.Deserialize<T>(jsonData);
        }
    }
}

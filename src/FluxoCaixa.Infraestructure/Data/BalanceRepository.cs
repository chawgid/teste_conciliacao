﻿using Dapper;
using FluxoCaixa.Infraestructure.Data.Interfaces;
using FluxoCaixa.Infraestructure.Models;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace FluxoCaixa.Infraestructure.Data
{
    public class BalanceRepository: IBalanceRepository
    {
        private readonly SqlConnection connection;
        public BalanceRepository(IConfiguration configuration)
        {
            this.connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection"));
        }
        public async Task<int> AddAsync(Balance entity)
        {
            var sql = "Insert into Balance (Value, Username,CompanyCode) VALUES (@Value,@Username,@CompanyCode)";

            connection.Open();
            var result = await connection.ExecuteAsync(sql, entity);
            connection.Close();
            return result;

        }
        public async Task<Balance> GetByDateAsync(string companyCode, string date)
        {
            var sql = "SELECT Id,Value,Username,CompanyCode FROM Balance WHERE CompanyCode = @CompanyCode AND Added = @Added";

            connection.Open();
            var result = await connection.QueryFirstAsync<Balance>(sql, new { CompanyCode = companyCode, Added = date });
            connection.Close();
            return result;

        }
    }
}

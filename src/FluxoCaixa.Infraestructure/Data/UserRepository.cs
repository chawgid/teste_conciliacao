﻿using Dapper;
using FluxoCaixa.Infraestructure.Data.Interfaces;
using FluxoCaixa.Infraestructure.Models;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace FluxoCaixa.Infraestructure.Data
{
    public class UserRepository: IUserRepository
    {
        private readonly SqlConnection connection;
        public UserRepository(IConfiguration configuration)
        {
            this.connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection"));
        }
        public async Task<int> AddAsync(User entity)
        {            
            var sql = "Insert into Users (Username,Password,Role,Email,CompanyCode) VALUES (@Username,@Password,@Role,@Email,@CompanyCode)";

            connection.Open();
            var result = await connection.ExecuteAsync(sql, entity);
            connection.Close();
            return result;

        }
        public async Task<User> GetByPassAsync(string username, string password)
        {
            var sql = "SELECT Id,Username,Role,Email,CompanyCode FROM Users WHERE Username = @Username AND Password = @Password";

            connection.Open();
            var result = await connection.QuerySingleOrDefaultAsync<User>(sql, new { Username = username, Password = password });
            connection.Close();
            return result;

        }

        public async Task<string> GetEmailAsync(string username)
        {
            var sql = "SELECT Email FROM Users WHERE Username = @Username";

            connection.Open();
            var result = await connection.QuerySingleOrDefaultAsync<string>(sql, new { Username = username});
            connection.Close();
            return result;

        }
    }
}

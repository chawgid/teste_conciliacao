﻿using Dapper;
using FluxoCaixa.Infraestructure.Data.Interfaces;
using FluxoCaixa.Infraestructure.Models;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace FluxoCaixa.Infraestructure.Data
{
    public class FlowRepository: IFlowRepository
    {
        private readonly SqlConnection connection;
        public FlowRepository(IConfiguration configuration)
        {
            this.connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection"));
        }
        public async Task<int> AddAsync(List<Flow> entity)
        {
            var sql = "Insert into Flow (Value,Type,Username,CompanyCode) VALUES (@Value,@Type,@Username,@CompanyCode)";

            connection.Open();
            var result = await connection.ExecuteAsync(sql, entity);
            connection.Close();
            return result;

        }
        public async Task<IEnumerable<Flow>> GetAllByDateAsync(string companyCode, string date)
        {
            var sql = "SELECT Id,Value,Type,Username,CompanyCode FROM Balance WHERE CompanyCode = @CompanyCode AND Added = @Added";

            connection.Open();
            var result = await connection.QueryAsync<Flow>(sql, new { CompanyCode = companyCode, Added = date });
            connection.Close();
            return result;

        }
    }
}

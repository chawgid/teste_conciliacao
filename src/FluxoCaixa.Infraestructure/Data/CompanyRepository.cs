﻿using Dapper;
using FluxoCaixa.Infraestructure.Data.Interfaces;
using FluxoCaixa.Infraestructure.Models;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace FluxoCaixa.Infraestructure.Data
{
    public class CompanyRepository: ICompanyRepository
    {
        private readonly SqlConnection connection;
        public CompanyRepository(IConfiguration configuration)
        {
            this.connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection"));
        }
        public async Task<int> AddAsync(Company entity)
        {
            var sql = "Insert into Companies (CompanyName,CompanyCode) VALUES (@CompanyName,@CompanyCode)";

            connection.Open();
            var result = await connection.ExecuteAsync(sql, entity);
            connection.Close();
            return result;

        }
        public async Task<Company> GetByCodeAsync(string companycode)
        {
            var sql = "SELECT Id,CompanyName,CompanyCode FROM Companies WHERE CompanyCode = @CompanyCode";

            connection.Open();
            var result = await connection.QuerySingleOrDefaultAsync<Company>(sql, new { CompanyCode = companycode });
            connection.Close();
            return result;

        }
    }
}

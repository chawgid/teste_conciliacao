﻿using FluxoCaixa.Infraestructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluxoCaixa.Infraestructure.Data.Interfaces
{
    public interface IUserRepository
    {
        Task<int> AddAsync(User entity);
        Task<User> GetByPassAsync(string username, string password);
        Task<string> GetEmailAsync(string username);
    }
}

﻿using FluxoCaixa.Infraestructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluxoCaixa.Infraestructure.Data.Interfaces
{
    public interface IBalanceRepository
    {
        Task<int> AddAsync(Balance entity);
        Task<Balance> GetByDateAsync(string companyCode, string date);
    }
}

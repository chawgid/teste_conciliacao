﻿using FluxoCaixa.Infraestructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluxoCaixa.Infraestructure.Data.Interfaces
{
    public interface IFlowRepository
    {
        Task<int> AddAsync(List<Flow> entity);
        Task<IEnumerable<Flow>> GetAllByDateAsync(string companyCode, string date);
    }
}

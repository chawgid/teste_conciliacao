﻿using FluxoCaixa.Infraestructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluxoCaixa.Infraestructure.Data.Interfaces
{
    public interface ICompanyRepository
    {
        Task<int> AddAsync(Company entity);
        Task<Company> GetByCodeAsync(string companycode);
    }
}

﻿namespace FluxoCaixa.Infraestructure.Models
{
    public class Balance
    {
        public int Id { get; set; }
        public float Value { get; set; }
        public string UserName { get; set; }
        public string CompanyCode { get; set; }
        public DateTime Added { get; set; }
    }
}

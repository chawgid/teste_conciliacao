﻿namespace FluxoCaixa.Infraestructure.Models
{
    public class Flow
    {
        public int Id { get; set; }
        public float Value { get; set; }
        public string Type { get; set; }
        public string Username { get; set; }
        public string CompanyCode { get; set; }
        public DateTime Added { get; set; }
    }
}

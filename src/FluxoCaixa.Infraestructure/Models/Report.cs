﻿namespace FluxoCaixa.Infraestructure.Models
{
    public class Report
    {
        public float LastBalance { get; set; }
        public List<Flow> Flows { get; set; } = new List<Flow>();
        public float BalanceActual { get; set; }
    }
}

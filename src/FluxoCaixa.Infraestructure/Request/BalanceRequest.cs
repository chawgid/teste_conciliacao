﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluxoCaixa.Infraestructure.Request
{
    public class BalanceRequest
    {
        public string UserName { get; set; }
        public string CompanyCode { get; set; }
        public DateTime Date { get; set; }
    }
}

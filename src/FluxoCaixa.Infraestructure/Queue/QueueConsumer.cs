﻿using RabbitMQ.Client.Events;
using System.Text;
using Microsoft.Extensions.Configuration;
using System.Text.Json;
using FluxoCaixa.Infraestructure.Data.Interfaces;
using FluxoCaixa.Infraestructure.Queue.Interfaces;
using FluxoCaixa.Infraestructure.Models;
using Microsoft.Extensions.Caching.Distributed;
using FluxoCaixa.Infraestructure.Request;
using RabbitMQ.Client;
using FluxoCaixa.Infraestructure.Helpers;
using System.Net.Mail;
using System.Net;

namespace FluxoCaixa.Infraestructure.Queue
{
    public class QueueConsumer : IQueueConsumer
    {
        private readonly IConfiguration _configuration;
        private readonly IBalanceRepository _balanceRepository;
        private readonly IFlowRepository _flowRepository;
        private readonly IUserRepository _userRepository;
        private readonly IDistributedCache _cache;
        private readonly ICacheHelper _cacheHelper;

        public QueueConsumer(IConfiguration configuration,
                                IBalanceRepository basicRepository,
                                IFlowRepository flowRepository,
                                IDistributedCache cache,
                                IUserRepository userRepository,
                                ICacheHelper cacheHelper)
        {
            _configuration = configuration;
            _balanceRepository = basicRepository;
            _flowRepository = flowRepository;
            _cache = cache;
            _cacheHelper = cacheHelper;
            _userRepository = userRepository;
        }

        public void ReceivePersistMessage()
        {
            var factory = new ConnectionFactory
            {
                HostName = _configuration.GetConnectionString("RabbitMQ")
            };

            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare(queue: _configuration["ReportQueueName"],
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);
            channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            Console.WriteLine(" [*] Waiting for messages.");

            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += async (sender, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                var balance = JsonSerializer.Deserialize<BalanceRequest>(message);
                Console.WriteLine(" [x] Received {0}", message);

                var result = 0;
                var email = string.Empty;
                if (balance != null)
                {
                    result = Execute(balance).Result;
                    
                    email = await _userRepository.GetEmailAsync(balance.UserName);
                    SendEmail(email);
                }




                if (result == 0)
                    throw new Exception();

                channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
            };
            channel.BasicConsume(queue: _configuration["ReportQueueName"],
                                 autoAck: false,
                                 consumer: consumer);
        }

        private async Task<int> Execute(BalanceRequest balance)
        {
            // get last balance
            var lastBalance = await _balanceRepository.GetByDateAsync(balance.CompanyCode, balance.Date.ToString("yyyy-MM-dd"));

            // get all flow
            var flows = await _flowRepository.GetAllByDateAsync(balance.CompanyCode, balance.Date.ToString("yyyy-MM-dd"));

            // calculate new balance
            var debitTotal = flows.Where(f => f.Type == "debit").Sum(x => x.Value);
            var creditTotal = flows.Where(f => f.Type == "credit").Sum(x => x.Value);

            var calculated = (lastBalance.Value + creditTotal) - debitTotal;

            var report = new Report
            {
                LastBalance = lastBalance.Value,
                Flows = flows.ToList(),
                BalanceActual = calculated
            };

            //save report on cache
            await _cacheHelper.SetRecordAsync<Report>(_cache, balance.CompanyCode, report);

            var newBalance = new Balance
            {
                Value = calculated,
                UserName = balance.UserName,
                CompanyCode = balance.CompanyCode
            };
          
            //save new balance into database
            var result = await _balanceRepository.AddAsync(newBalance);
            
            return balance != null ? result : 0;
        }

        private static void SendEmail(string emailUsuario)
        {
            SmtpClient client = new("Host")
            {
                Credentials = new NetworkCredential("username", "password")
            };

            MailMessage mailMessage = new()
            {
                From = new MailAddress("sender@gmail.com"),
                Body = "Processamento do relatório saldo diário contabilizado foi finalizado com sucesso, pode consulta-lo.",
                Subject = "Processamento de relatório saldo diário"
            };
            mailMessage.To.Add(emailUsuario);
            client.Send(mailMessage);
        }
    }
}

﻿namespace FluxoCaixa.Infraestructure.Queue.Interfaces
{
    public interface IQueueProducer
    {
        public void SendMessage(string message);
    }
}

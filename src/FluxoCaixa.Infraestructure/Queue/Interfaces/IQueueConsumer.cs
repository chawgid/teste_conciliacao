﻿namespace FluxoCaixa.Infraestructure.Queue.Interfaces
{
    public interface IQueueConsumer
    {
        void ReceivePersistMessage();
    }
}

﻿using FluxoCaixa.Infraestructure.Queue.Interfaces;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using System.Text;

namespace FluxoCaixa.Infraestructure.Queue
{
    public class QueueProducer : IQueueProducer
    {
        private readonly IConfiguration _configuration;

        public QueueProducer(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void SendMessage(string message)
        {
            var factory = new ConnectionFactory() { HostName = _configuration.GetConnectionString("RabbitMQ") };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare(queue: _configuration["ReportQueueName"],
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            var body = Encoding.UTF8.GetBytes(message);

            var properties = channel.CreateBasicProperties();
            properties.Persistent = true;

            channel.BasicPublish(exchange: "",
                                 routingKey: _configuration["ReportQueueName"],
                                 basicProperties: properties,
                                 body: body);
            Console.WriteLine(" [x] Sent {0}", message);
        }
    }
}

﻿using FluxoCaixa.Core.Interfaces;
using System.Diagnostics.CodeAnalysis;

namespace FluxoCaixa.Report.Worker
{
    [ExcludeFromCodeCoverage]
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IReportService _reportService;

        public Worker(ILogger<Worker> logger, IReportService reportService)
        {
            _logger = logger;
            _reportService = reportService;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);

                _reportService.ReceivePersistMessage();

                await Task.Delay(1000, stoppingToken);
            }
        }
    }
}

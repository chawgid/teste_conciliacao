using FluxoCaixa.Core.Interfaces;
using FluxoCaixa.Core.Services;
using FluxoCaixa.Infraestructure.Data.Interfaces;
using FluxoCaixa.Infraestructure.Data;
using FluxoCaixa.Report.Worker;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddScoped<IReportService, ReportService>();
builder.Services.AddScoped<IBalanceService, BalanceService>();
builder.Services.AddScoped<IFlowService, FlowService>();

builder.Services.AddScoped<IBalanceRepository, BalanceRepository>();
builder.Services.AddScoped<IFlowRepository, FlowRepository>();

builder.Services.AddControllers();
builder.Services.AddHealthChecks()
.AddSqlServer(connectionString: builder.Configuration.GetConnectionString("DefaultConnection"),
name: "Instancia do Sql Server");

builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseRouting();
app.UseHealthChecks("/health");

app.Run();

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices(services =>
    {
        services.AddHostedService<Worker>();
    })
    .Build();

await host.RunAsync();

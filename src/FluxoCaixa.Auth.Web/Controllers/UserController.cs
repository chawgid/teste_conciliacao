﻿using FluxoCaixa.Core.Interfaces;
using FluxoCaixa.Infraestructure.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics.CodeAnalysis;

namespace FluxoCaixa.Auth.Web.Controllers;

[ApiController]
[ExcludeFromCodeCoverage]
public class UserController : Controller
{
    private readonly IUserService _userService;
    private readonly ITokenService _tokenService;
    public UserController(IUserService userService, ITokenService tokenService)
    {
        _userService = userService;
        _tokenService = tokenService;
    }

    [Route("login")]
    [HttpGet]
    public ActionResult<dynamic> Authenticate([FromBody] User model)
    {
        var user = _userService.GetByPassAsync(model.Username, model.Password).Result;

        if (user == null)
            return NotFound(new { message = "Usuário ou senha inválidos" });

        var token = _tokenService.GenerateToken(user);

        // Retorna os dados
        return new
        {
            user,
            token
        };
    }
            
    [Route("create")]
    [Authorize(Roles = "manager")]
    [HttpPost]
    public ActionResult Create([FromBody] User user)
    {
        if (user is null)
        {
            return BadRequest();
        }

        var result = _userService.AddAsync(user).Result;

        if (result == 0)
        {
            return BadRequest();
        }

        return Ok();
    }
}

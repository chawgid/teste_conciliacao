﻿using FluxoCaixa.Core.Interfaces;
using FluxoCaixa.Infraestructure.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics.CodeAnalysis;

namespace FluxoCaixa.Auth.Web.Controllers
{
    [ApiController]
    [ExcludeFromCodeCoverage]
    public class CompanyController : Controller
    {
        private readonly ICompanyService _companyService;
        public CompanyController(ICompanyService companyService)
        {
            _companyService = companyService;
        }

        [Route("create")]
        [Authorize(Roles = "manager")]
        [HttpPost]
        public ActionResult Create([FromBody] Company company)
        {
            if (company is null)
            {
                return BadRequest();
            }

            var result = _companyService.AddAsync(company).Result;

            if (result == 0)
            {
                return BadRequest();
            }

            return Ok();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluxoCaixa.Core.Interfaces;
using FluxoCaixa.Infraestructure.Data.Interfaces;
using FluxoCaixa.Infraestructure.Models;

namespace FluxoCaixa.Core.Services
{
    public class UserService: IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<User> GetByPassAsync(string username, string password)
        {
            return await _userRepository.GetByPassAsync(username, password);
        }

        public async Task<int> AddAsync(User user)
        {
            return await _userRepository.AddAsync(user);
        }
    }
}

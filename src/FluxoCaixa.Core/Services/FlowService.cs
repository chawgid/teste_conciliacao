﻿using FluxoCaixa.Core.Interfaces;
using FluxoCaixa.Infraestructure.Data.Interfaces;
using FluxoCaixa.Infraestructure.Models;

namespace FluxoCaixa.Core.Services
{
    public class FlowService: IFlowService
    {
        private readonly IFlowRepository _flowRepository;
        public FlowService(IFlowRepository flowRepository)
        {
            _flowRepository = flowRepository;
        }

        public async Task<IEnumerable<Flow>> GetAllByDateAsync(string companyCode, DateTime date)
        {
            return await _flowRepository.GetAllByDateAsync(companyCode, date.ToString("yyyy-MM-dd"));
        }

        public async Task<int> AddAsync(List<Flow> flow)
        {
            return await _flowRepository.AddAsync(flow);
        }
    }
}

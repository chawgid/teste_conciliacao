﻿using FluxoCaixa.Core.Interfaces;
using FluxoCaixa.Infraestructure.Data.Interfaces;
using FluxoCaixa.Infraestructure.Models;

namespace FluxoCaixa.Core.Services
{
    public class BalanceService: IBalanceService
    {
        private readonly IBalanceRepository _balanceRepository;
        public BalanceService(IBalanceRepository balanceRepository)
        {
            _balanceRepository = balanceRepository;
        }

        public async Task<Balance> GetByDateAsync(string companyCode, DateTime date)
        {
            return await _balanceRepository.GetByDateAsync(companyCode, date.ToString("yyyy-MM-dd"));
        }

        public async Task<int> AddAsync(Balance balance)
        {
            return await _balanceRepository.AddAsync(balance);
        }
    }
}

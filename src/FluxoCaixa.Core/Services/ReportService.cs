﻿using FluxoCaixa.Core.Interfaces;
using FluxoCaixa.Infraestructure.Helpers;
using FluxoCaixa.Infraestructure.Models;
using FluxoCaixa.Infraestructure.Queue.Interfaces;
using FluxoCaixa.Infraestructure.Request;
using Microsoft.Extensions.Caching.Distributed;
using System.Text.Json;

namespace FluxoCaixa.Core.Services
{
    public class ReportService: IReportService
    {
        private readonly IQueueProducer _queueProducer;
        private readonly IQueueConsumer _queueConsumer;
        private IDistributedCache _cache;
        private readonly ICacheHelper _cacheHelper;
        public ReportService(IQueueProducer queueProducer, 
                                IQueueConsumer queueConsumer, 
                                IDistributedCache cache,
                                ICacheHelper cacheHelper)
        {
            _queueProducer = queueProducer;
            _queueConsumer = queueConsumer;
            _cache = cache;
            _cacheHelper = cacheHelper;
        }

        public void SendMessage(BalanceRequest balance)
        {
            var json = JsonSerializer.Serialize(balance);
            _queueProducer.SendMessage(json);
        }

        public void ReceivePersistMessage()
        {
            _queueConsumer.ReceivePersistMessage();
        }

        public async Task<string> GetReportAsync(string CompanyCode)
        {
            var report = await _cacheHelper.GetRecordAsync<Report>(_cache, CompanyCode);
            var json = JsonSerializer.Serialize(report);
           return json;
        }
    }
}

﻿using FluxoCaixa.Infraestructure.Models;
using FluxoCaixa.Infraestructure.Request;

namespace FluxoCaixa.Core.Interfaces
{
    public interface IReportService
    {
        void SendMessage(BalanceRequest balance);
        void ReceivePersistMessage();
        Task<string> GetReportAsync(string CompanyCode);
    }
}

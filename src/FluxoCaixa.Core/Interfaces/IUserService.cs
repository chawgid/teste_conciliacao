﻿using FluxoCaixa.Infraestructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluxoCaixa.Core.Interfaces
{
    public interface IUserService
    {
        Task<User> GetByPassAsync(string username, string password);
        Task<int> AddAsync(User user);
    }
}

﻿using FluxoCaixa.Infraestructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluxoCaixa.Core.Interfaces
{
    public interface IFlowService
    {
        Task<IEnumerable<Flow>> GetAllByDateAsync(string companyCode, DateTime date);
        Task<int> AddAsync(List<Flow> flow);
    }
}

﻿using FluxoCaixa.Infraestructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluxoCaixa.Core.Interfaces
{
    public interface IBalanceService
    {
        Task<Balance> GetByDateAsync(string companyCode, DateTime date);
        Task<int> AddAsync(Balance balance);
    }
}

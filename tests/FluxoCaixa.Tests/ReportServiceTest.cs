﻿using FluxoCaixa.Core.Interfaces;
using FluxoCaixa.Core.Services;
using FluxoCaixa.Infraestructure.Data.Interfaces;
using FluxoCaixa.Infraestructure.Helpers;
using FluxoCaixa.Infraestructure.Models;
using FluxoCaixa.Infraestructure.Queue;
using FluxoCaixa.Infraestructure.Queue.Interfaces;
using FluxoCaixa.Infraestructure.Request;
using Microsoft.Extensions.Caching.Distributed;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace FluxoCaixa.Tests
{
    public class ReportServiceTest
    {
        public readonly Mock<IQueueProducer> queueProducer;
        public readonly Mock<IQueueConsumer> queueConsumer;
        public readonly Mock<IDistributedCache> cache;
        public readonly Mock<ICacheHelper> cacheHelper;
        public readonly IReportService reportService;

        public ReportServiceTest()
        {
            queueProducer = new Mock<IQueueProducer>();
            queueConsumer = new Mock<IQueueConsumer>();
            cache = new Mock<IDistributedCache>();
            cacheHelper = new Mock<ICacheHelper>();
            reportService = new ReportService(queueProducer.Object, queueConsumer.Object, cache.Object, cacheHelper.Object);

            var report = new Report()
            {
                LastBalance = 12345F,
                Flows = new List<Flow>()
                        {
                            new Flow()
                            {
                                Id = 1,
                                Value = 123F,
                                Type = "credit",
                                Username = "testUser01",
                                CompanyCode = "codetest0001",
                                Added = DateTime.Now

                            },
                            new Flow()
                            {
                                Id = 2,
                                Value = 32F,
                                Type = "debit",
                                Username = "testUser01",
                                CompanyCode = "codetest0001",
                                Added = DateTime.Now
                            },
                         },
                BalanceActual = 56123312F

            };

            var json = JsonSerializer.Serialize(report);

            queueProducer.Setup(x => x.SendMessage(It.IsAny<string>()));
            queueConsumer.Setup(x => x.ReceivePersistMessage());
            cacheHelper.Setup(x => x.GetRecordAsync<Report>(It.IsAny<IDistributedCache>(), It.IsAny<string>())).ReturnsAsync(report);
            cacheHelper.Setup(x => x.SetRecordAsync(It.IsAny<IDistributedCache>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(json));
        }

        [Fact]
        public void SendMessageTest()
        {
            var request = new BalanceRequest()
            {
                UserName = "testUser01",
                CompanyCode = "codetest0001",
                Date = DateTime.Now
            };
            reportService.SendMessage(request);

            queueProducer.Verify(x => x.SendMessage(It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void ReceivePersistMessageTest()
        {
            reportService.ReceivePersistMessage();
            queueConsumer.Verify(x => x.ReceivePersistMessage(), Times.Once);
        }

        [Fact]
        public void GetReportAsyncTest()
        {
            var json = reportService.GetReportAsync("codetest0001").Result;

            var report = JsonSerializer.Deserialize<Report>(json);

            Assert.Equal(2, report?.Flows.Count());
            Assert.Equal(12345F, report?.LastBalance);
        }
    }
}

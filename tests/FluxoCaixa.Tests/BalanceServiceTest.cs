﻿
using FluxoCaixa.Core.Interfaces;
using FluxoCaixa.Core.Services;
using FluxoCaixa.Infraestructure.Data.Interfaces;
using FluxoCaixa.Infraestructure.Models;
using Moq;

namespace FluxoCaixa.Tests
{
    public class BalanceServiceTest
    {
        public readonly Mock<IBalanceRepository> balanceRepository;
        public readonly IBalanceService balanceService;
        public BalanceServiceTest()
        {
            balanceRepository = new Mock<IBalanceRepository>();
            balanceService = new BalanceService(balanceRepository.Object);
            var balance = new Balance()
            {
                Id = 1,
                Value = 123F,
                UserName = "testUser01",
                CompanyCode = "codetest0001",
                Added = DateTime.Now
            };

            balanceRepository.Setup(x => x.GetByDateAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(balance);

        }

        [Fact]
        public void GetByDateAsyncTest()
        {
            var basic = balanceService.GetByDateAsync("codetest0001", DateTime.Now).Result;

            Assert.Equal(1, basic.Id);
        }

        [Fact]
        public void AddAsyncTest()
        {
            balanceRepository.Setup(x => x.AddAsync(It.IsAny<Balance>())).ReturnsAsync(1);

            var balance = new Balance()
            {
                Id = 1,
                Value = 123F,
                UserName = "testUser01",
                CompanyCode = "codetest0001",
                Added = DateTime.Now
            };

            var result = balanceService.AddAsync(balance).Result;

            Assert.Equal(1, result);
        }
    }
}

﻿using FluxoCaixa.Core.Interfaces;
using FluxoCaixa.Core.Services;
using FluxoCaixa.Infraestructure.Data.Interfaces;
using FluxoCaixa.Infraestructure.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluxoCaixa.Tests
{
    public class FlowServiceTest
    {
        public readonly Mock<IFlowRepository> flowRepository;
        public readonly IFlowService flowService;
        public FlowServiceTest()
        {
            flowRepository = new Mock<IFlowRepository>();
            flowService = new FlowService(flowRepository.Object);

            var flows = new List<Flow>()
            {
                new Flow()
                {
                    Id = 1,
                    Value = 123F,
                    Type = "credit",
                    Username = "testUser01",
                    CompanyCode = "codetest0001",
                    Added = DateTime.Now

                },
                new Flow()
                {
                    Id = 2,
                    Value = 32F,
                    Type = "debit",
                    Username = "testUser01",
                    CompanyCode = "codetest0001",
                    Added = DateTime.Now
                },
             };

            flowRepository.Setup(x => x.GetAllByDateAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(flows);
        }

        [Fact]
        public void GetByDateAsyncTest()
        {
            var flows = flowService.GetAllByDateAsync("codetest0001", DateTime.Now).Result;

            Assert.Equal(2, flows.Count()); 
            Assert.Equal(1, flows.First().Id);
        }

        [Fact]
        public void AddAsyncTest()
        {
            flowRepository.Setup(x => x.AddAsync(It.IsAny<List<Flow>>())).ReturnsAsync(1);

            var flows = new List<Flow>()
            {
                new Flow()
                {
                    Id = 1,
                    Value = 123F,
                    Type = "credit",
                    Username = "testUser01",
                    CompanyCode = "codetest0001",
                    Added = DateTime.Now

                },
                new Flow()
                {
                    Id = 2,
                    Value = 32F,
                    Type = "debit",
                    Username = "testUser01",
                    CompanyCode = "codetest0001",
                    Added = DateTime.Now
                },
             };

            var result = flowService.AddAsync(flows).Result;

            Assert.Equal(1, result);
        }
    }
}

﻿using FluxoCaixa.Core.Interfaces;
using FluxoCaixa.Core.Services;
using FluxoCaixa.Infraestructure.Data.Interfaces;
using FluxoCaixa.Infraestructure.Models;
using Moq;

namespace FluxoCaixa.Tests
{
    public class UserServiceTest
    {
        public readonly Mock<IUserRepository> userRepository;
        public readonly IUserService userService;
      
        public UserServiceTest()
        {
            userRepository = new Mock<IUserRepository>();
            userService = new UserService(userRepository.Object);
            var user = new User()
            {
                Id = 1,
                Username = "testUser01",
                Password = "123@456",
                Role = "employee",
                Email = "teste@email.com",
                CompanyCode = "codetest0001"
            };

            userRepository.Setup(x => x.GetByPassAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(user);
        }

        [Fact]
        public void GetByPassAsyncTest()
        {
            var user = userService.GetByPassAsync("codetest0001", "123@456").Result;

            Assert.Equal(1, user.Id);
        }

        [Fact]
        public void AddAsyncTest()
        {
            userRepository.Setup(x => x.AddAsync(It.IsAny<User>())).ReturnsAsync(1);

            var user = new User()
            {
                Id = 1,
                Username = "testUser01",
                Password = "123@456",
                Role = "employee",
                Email = "teste@email.com",
                CompanyCode = "codetest0001"
            };

            var result = userService.AddAsync(user).Result;

            userRepository.Verify(x => x.AddAsync(It.IsAny<User>()), Times.Once);
            Assert.Equal(1, result);
        }
    }
}

﻿using FluxoCaixa.Core.Interfaces;
using FluxoCaixa.Core.Services;
using FluxoCaixa.Infraestructure.Data;
using FluxoCaixa.Infraestructure.Data.Interfaces;
using FluxoCaixa.Infraestructure.Models;
using Moq;

namespace FluxoCaixa.Tests
{
    public class CompanyServiceTest
    {
        public readonly Mock<ICompanyRepository> companyRepository;
        public readonly ICompanyService companyService;
        public CompanyServiceTest()
        {
            companyRepository = new Mock<ICompanyRepository>();
            companyService = new CompanyService(companyRepository.Object);
            

        }

        [Fact]
        public void AddAsyncTest()
        {
            companyRepository.Setup(x => x.AddAsync(It.IsAny<Company>())).ReturnsAsync(1);

            var company = new Company()
            {
                Id = 1,
                CompanyName = "testCompany01",
                CompanyCode = "codetest0001"
            };

            var result = companyService.AddAsync(company).Result;

            Assert.Equal(1, result);
        }
    }
}
